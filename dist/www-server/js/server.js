"use strict";
var express = require("express");
var federation_controller_1 = require("./controllers/federation-controller");
/** Server for impersonation and federation. */
var ServerFactory = (function () {
    function ServerFactory() {
        this.express = express();
        this.mountRoutes();
    }
    /**
     * In this case we mount only one route for handling the federation request.
     */
    ServerFactory.prototype.mountRoutes = function () {
        this.express.use("/federate", federation_controller_1.federationController);
    };
    return ServerFactory;
}());
module.exports = new ServerFactory().express;
