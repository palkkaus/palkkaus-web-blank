"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = require("jsonwebtoken");
/** Utility class for creating an assertion token from the certificate for given account */
var CertificateUtility = (function () {
    function CertificateUtility() {
    }
    CertificateUtility.createAssertionToken = function (thumbprint, certificate, privateKey, issuer, expires, accountId) {
        // Read and parse certificate
        var crtData = certificate.toString();
        var startTag = "-----BEGIN CERTIFICATE-----";
        var endTag = "-----END CERTIFICATE-----";
        var start = crtData.indexOf(startTag) + startTag.length;
        var end = crtData.indexOf(endTag);
        crtData = crtData.substring(start, end).replace(/(?:\r\n|\r|\n)/g, "");
        // Make header with certificate data
        var buffer = new Buffer(thumbprint, "hex");
        var x5t = buffer.toString("base64");
        x5t = x5t.replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");
        var headers = {
            algorithm: "RS256",
            header: {
                x5t: x5t,
                x5c: [crtData],
            },
        };
        // Make payload
        var payload = {
            iss: issuer,
            sub: accountId,
            aud: "https://test-api.salaxy.com/oauth2/token",
            exp: Math.floor(expires.getTime() / 1000),
        };
        // Sign certificate
        var token = jsonwebtoken_1.sign(payload, privateKey, headers);
        return token;
    };
    return CertificateUtility;
}());
exports.CertificateUtility = CertificateUtility;
