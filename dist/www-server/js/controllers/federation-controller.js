"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var fs = require("fs");
var path = require("path");
var certificate_utility_1 = require("../helpers/certificate-utility");
var core_1 = require("@salaxy/core");
/** Controller for impersonation and federation. */
var ControllerFactory = (function () {
    function ControllerFactory() {
        this.controller = this.router();
        // Stores user data in this site
        this.userDatabase = "dist/www-server/storage/users.json";
        // Certificate store
        this.certificateStore = "dist/www-server/storage/certificates";
        // The thumbprint of the Salaxy certificate which is used for impersonation
        this.salaxyCertificateThumbprint = "dc68d8bc89043173fd316f34b93599085147c74f";
        // Our own Salaxy account id
        this.ownSalaxyAccountId = "FI14POYT0002345678";
    }
    /**
     *  Please note that in this sample we give the userId as a parameter.
     *  In the real life, however, the userId must be obtained in a secure way after proper authorization.
     */
    ControllerFactory.prototype.router = function () {
        var _this = this;
        var ctrl = express_1.Router();
        // Federate the given user and redirect the user with the new token to the site root
        ctrl.get("/:userId", function (req, res) {
            // Get userId parameter
            var userId = req.params.userId;
            // Federate the user
            return _this.federate(userId)
                .then(function (token) {
                // Redirect to the site root with the new token
                return res.redirect("/#access_token=" + token);
            });
        });
        return ctrl;
    };
    /** Federates the given company user and returns a new token for the user. */
    ControllerFactory.prototype.federate = function (userId) {
        var _this = this;
        // First assure that the company will be created if it does not exist
        return this.assureCompanyAccount(userId)
            .then(function (company) {
            // Create assertion token for the company using Salaxy account id
            var assertionToken = _this.getAssertionToken(company.salaxyAccountId);
            // Request a new token for the company using the assertion token
            return _this.getToken(assertionToken);
        })
            .then(function (tokenMessage) {
            // Return the received token
            return tokenMessage.access_token;
        });
    };
    /** Create assertion token for given Salaxy account id */
    ControllerFactory.prototype.getAssertionToken = function (salaxyAccountId) {
        // The thumbprint of the certificate
        var thumbprint = this.salaxyCertificateThumbprint;
        // This can be partner specific
        var issuer = "http://tempuri.org";
        var expires = new Date(Date.now() + (10 * 24 * 60 * 60 * 1000));
        var baseFilePath = path.resolve(this.certificateStore, thumbprint);
        var crt = fs.readFileSync(baseFilePath + ".crt");
        var pvk = fs.readFileSync(baseFilePath + ".pvk");
        var assertionToken = certificate_utility_1.CertificateUtility.createAssertionToken(thumbprint, crt, pvk, issuer, expires, salaxyAccountId);
        return assertionToken;
    };
    /** Get bearer token using assertion */
    ControllerFactory.prototype.getToken = function (assertionToken) {
        var oauth2 = new core_1.OAuth2(this.getAjax());
        var oAuthRequest = {
            grant_type: core_1.OAuthGrantType.JwtBearer,
            assertion: assertionToken,
        };
        return oauth2.token(oAuthRequest);
    };
    /** Assures that the company will be created if it does not exist */
    ControllerFactory.prototype.assureCompanyAccount = function (userId) {
        var _this = this;
        // Check if the user exists in our own database
        var user = this.getUserInfo(userId);
        if (user == null) {
            throw Error("this should never happen");
        }
        // Create assertion token for self
        var assertionToken = this.getAssertionToken(this.ownSalaxyAccountId);
        // Use assertion token to generate a new token for self
        return this.getToken(assertionToken)
            .then(function (tokenMessage) {
            // Configure the API client with the token
            var ajax = _this.getAjax();
            ajax.setCurrentToken(tokenMessage.access_token);
            // Create request object
            var assureCompanyAccountRequest = {
                officialId: user.officialId,
                partnerAccountId: user.userId,
            };
            // Post data to API
            return ajax.postJSON("/partner/company", assureCompanyAccountRequest);
        })
            .then(function (partnerCompanyAccountInfo) {
            // Save the returned data to our own user database
            user.salaxyAccountId = partnerCompanyAccountInfo.accountId;
            _this.saveUserInfo(user);
            return user;
        });
    };
    /** Returns the API client */
    ControllerFactory.prototype.getAjax = function () {
        var ajax = new core_1.AjaxNode();
        ajax.serverAddress = "https://test-api.salaxy.com"; // For production use: https://secure-salaxy-com
        return ajax;
    };
    /** Returns a user from the user database. */
    ControllerFactory.prototype.getUserInfo = function (userId) {
        var data = fs.readFileSync(path.resolve(this.userDatabase));
        var users = JSON.parse(data.toString("utf8"));
        for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
            var user = users_1[_i];
            if (user.userId === userId) {
                return user;
            }
        }
        return null;
    };
    /** Saves user to the user database. */
    ControllerFactory.prototype.saveUserInfo = function (user) {
        var data = fs.readFileSync(path.resolve(this.userDatabase));
        var users = JSON.parse(data.toString("utf8"));
        for (var i = 0; i < users.length; i++) {
            if (users[i].userId === user.userId) {
                users[i] = user;
                break;
            }
        }
        fs.writeFileSync(path.resolve(this.userDatabase), JSON.stringify(users));
    };
    return ControllerFactory;
}());
exports.federationController = new ControllerFactory().controller;
