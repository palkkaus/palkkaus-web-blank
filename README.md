
PALKKAUS / SALAXY STARTER SITE (palkkaus-web-blank)
===================================================

Clean Palkkaus.fi web site project. This is the simplest way to start developing a new web site based on 
Salaxy API and using our Palkkaus.fi Angular libraries. 

The project contains additionally an express server for demonstraing external user federation.

Getting started
---------------

1. Clone repository `git clone https://gitlab.com/palkkaus/palkkaus-web-blank`
2. Enter project directory `cd palkkaus-web-blank`
3. Run `npm install` for installing required grunt-modules
3. Start the project web site in dev web server: `grunt server`
  - Server starts in the port 9001
4. Have fun in exploring and developing with the code.
