/**
 * Main angular site
 */

"use strict";
angular.module("Salaxy.app", ["ngRoute", "salaxy.ng1.components.all"])
    .config(["$routeProvider",
        function($routeProvider) {

            /* YOUR ROUTING LOGIC HERE - THIS IS JUST AN EXAMPLE */

            // Helper to get a path to template with defaults
            function getTemplate(section, template?) {

                return "/app/views/" + section + "/" + (template || "index") + ".html";
            }

            $routeProvider
                .when("/", { templateUrl: getTemplate("home") })
                .when("/error", { templateUrl: getTemplate("home", "error") })
                .when("/calculations/:viewName", { templateUrl: function(path) { return getTemplate("calculations", path.viewName); }})
                .when("/calculations/pro/:calculationId", { templateUrl: getTemplate("calculations", "pro") })
                .when("/calculations/detail/:calculationId", { templateUrl: getTemplate("calculations", "detail") })
                .when("/calculations/newFor/:newCalcForWorkerId", { templateUrl: getTemplate("calculations", "detail") })
                .when("/workers/:viewName", { templateUrl: function(path) { return getTemplate("workers", path.viewName); }})
                .when("/workers/:viewName/:workerId", { templateUrl: function(path) { return getTemplate("workers", path.viewName); }})
                .when("/products/:viewName", { templateUrl: function(path) { return getTemplate("products", path.viewName); }})
                .when("/products/info/:productId", { templateUrl: getTemplate("products", "info") })
                .when("/reports/:reportType", { templateUrl: getTemplate("reports") })
                .when("/cms/index", { templateUrl: getTemplate("cms") })
                .when("/cms/article/:articleId", { templateUrl: getTemplate("cms", "article") })
                .otherwise({ templateUrl: getTemplate("home", "error404") });
        },
    ])
    .constant("SITEMAP", [{
        title: "Demo home",
        url: "#/",
        id: "home",
    },
    {
        title: "Laskelmat",
        url: "#/calculations/index",
        id: "calculations",
        children: [
            {
                title: "Uusi laskelma",
                url: "#/calculations/detail/new",
                isFullWidth: true,
            },
            {
                title: "Palkkalaskelma",
                url: "#/calculations/detail/*",
                hidden: true,
                isFullWidth: true,
            },
            {
                title: "Pro palkkalaskelma",
                url: "#/calculations/pro/*",
                hidden: true,
                isFullWidth: true,
            },
            {
                title: "Uusi laskelma työntekijälle",
                url: "#/calculations/newFor/*",
                hidden: true,
                isFullWidth: true,
            },
        ],
    },
    {
        title: "Työntekijät",
        url: "#/workers/index",
        id: "workers",
        children: [
            {
                title: "Uusi työntekijä",
                url: "#/workers/detail/new",
                isFullWidth: true,
            },
            {
                title: "Työntekijän tiedot",
                url: "#/workers/detail/*",
                hidden: true,
                isFullWidth: true,
            },
            {
                title: "Työntekijän laskelmat",
                url: "#/workers/calculations/*",
                hidden: true,
                isFullWidth: true,
            },
            {
                title: "Verokortit",
                url: "#/workers/taxcards/*",
                hidden: true,
                isFullWidth: true,
            },
            {
                title: "Työsuhde",
                url: "#/workers/employment/*",
                hidden: true,
                isFullWidth: true,
            },
        ],
    },
    {
        title: "Palkkaus-palvelut",
        url: "#/products/index",
        id: "products",
        children: [
            {
                title: "Tietoa palvelusta",
                url: "#/products/info",
            },
            {
                title: "Palkkaus.fi-tilin tiedot",
                url: "#/products/accountInfo",
            },
            {
                title: "Käyttäjätunnuksen tiedot",
                url: "#/products/userInfo",
            },
        ],
    },
    {
        title: "Raportit",
        url: "#/reports/monthlyDetails",
        id: "reports",
        children: [
            {
                title: "Kuukausiraportti",
                url: "#/reports/monthlyDetails",
            },
            {
                title: "Palkkalaskelmat",
                url: "#/reports/salarySlipPaid",
            },
            {
                title: "Veron KK-raportti (4001)",
                url: "#/reports/taxMonthly4001",
            },
            {
                title: "Veron vuosiraportti (7801)",
                url: "#/reports/taxYearly7801",
            },
            {
                title: "Työntekijän vuosiyhteenveto",
                url: "#/reports/yearlyWorkerSummary",
            },
            {
                title: "Vuosiraportti (yksityiskohdat)",
                url: "#/reports/yearlyDetails",
            },
            {
                title: "Kirjanpitoraportti (Liikekirjuri)",
                url: "#/reports/monthlyLiikekirjuri",
            },
            {
                title: "Kirjanpitoraportti (Rapko)",
                url: "#/reports/monthlyRapko",
            },

            {
                title: "Vuosiraportti (data/JSON)",
                url: "#/reports/yearEndReport",
            },
            {
                title: "TVR-ilmoitus",
                url: "#/reports/unemployment",
            },
            {
                title: "Vakuutusraportti",
                url: "#/reports/insurance",
            },
            {
                title: "Kotitalousvähennys (14B)",
                url: "#/reports/taxHouseholdDeduction14B",
            },
            {
                title: "Palkkalaskelma (TT/vastaanotetut)",
                url: "#/reports/salarySlip",
            },
        ],
    }])
    .run(["$rootScope", "$route", "$routeParams", "$window", "CalculationsService", "WorkersService", "AccountService", "CmsService", "ReportsService", "OnboardingService", "SessionService", "AjaxNg1",
        function($rootScope, $route, $routeParams, $window, calculationsService, workersService, accountService, cmsService, reportsService, onboardingService, sessionService, ajax) {
            // Show the Onborading if signature has not been done.
            sessionService.onAuthenticatedSession($rootScope, function() {

                if (!sessionService.getSession().currentAccount) {
                    return;
                }
                if (
                    sessionService.getSession().currentAccount.identity.contract &&
                    sessionService.getSession().currentAccount.identity.contract.isSigned
                ) {
                    return;
                }
                const url = ajax.getServerAddress() + "/Onboarding/new?access_token=" + ajax.getCurrentToken();
                $window.location.href = url;

            });

            /* YOUR ROUTING LOGIC HERE - THIS IS JUST AN EXAMPLE */

            // Handle the parameters coming in route parameters (typically selected business object id)
            const checkRoute = function() {
                if ($routeParams.newCalcForWorkerId) {
                    calculationsService.createNew($routeParams.newCalcForWorkerId);
                }
                if ($routeParams.calculationId) {
                    calculationsService.setCurrent($routeParams.calculationId, true);
                }
                if ($routeParams.workerId) {
                    workersService.setCurrent($routeParams.workerId, true);
                }
                if ($routeParams.productId) {
                    accountService.setCurrent($routeParams.productId);
                }
                if ($routeParams.articleId) {
                    cmsService.setCurrent($routeParams.articleId);
                }
                if ($routeParams.reportType) {
                    reportsService.currentReportType = $routeParams.reportType;
                }
            };
            $rootScope.$on("$routeChangeSuccess", checkRoute);
        },
    ]);
