The main purpose of this server is to act as a federation service for the single page app.

The service uses storage/users.json - "database" as a user store.

The federeration simply takes place by requesting url /federate/{userId}, where the userId is the id of an existing user in the external system (e.g. 501). The users.json contains the officialId for the user. The federation has the following steps:

- the Salaxy account will be created based on the officialId if it does not exist in the Salaxy system
- access_token will be generated
- the end user will be redirected to the single page application with access_token
- end end user will be shown the onboarding wizard for verification