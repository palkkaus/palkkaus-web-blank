import * as express from "express";
import { federationController } from "./controllers/federation-controller";

/** Server for impersonation and federation. */
class ServerFactory {
  public express: express.Express;

  constructor() {
    this.express = express();
    this.mountRoutes();
  }

  /**
   * In this case we mount only one route for handling the federation request.
   */
  private mountRoutes(): void {
    this.express.use("/federate", federationController);
  }
}

export = new ServerFactory().express;
