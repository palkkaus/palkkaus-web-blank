
import {sign} from "jsonwebtoken";

/** Utility class for creating an assertion token from the certificate for given account */
export class CertificateUtility {

    public static createAssertionToken(thumbprint: string, certificate: Buffer, privateKey: Buffer, issuer: string, expires: Date, accountId: string): string {

        // Read and parse certificate
        let crtData = certificate.toString();
        const startTag = "-----BEGIN CERTIFICATE-----";
        const endTag = "-----END CERTIFICATE-----";
        const start = crtData.indexOf(startTag) + startTag.length;
        const end = crtData.indexOf(endTag);
        crtData = crtData.substring(start, end).replace(/(?:\r\n|\r|\n)/g, "");

        // Make header with certificate data
        const buffer = new Buffer(thumbprint, "hex");
        let x5t = buffer.toString("base64");
        x5t = x5t.replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");

        const headers = {
            algorithm: "RS256",
            header: {
                x5t,
                x5c: [crtData],
            },
        };

        // Make payload
        const payload = {
            iss: issuer,
            sub: accountId,
            aud: "https://test-api.salaxy.com/oauth2/token", // For production use: "https://secure.salaxy.com/oauth2/token"
            exp: Math.floor(expires.getTime() / 1000),
        };
        // Sign certificate
        const token = sign(payload, privateKey, headers);
        return token;
    }
}
