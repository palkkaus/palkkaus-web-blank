import { Request, Response, Router } from "express";
import * as fs from "fs";
import * as path from "path";
import { CertificateUtility } from "../helpers/certificate-utility";
import { UserInfo } from "../models/user-info";

import {
  AjaxNode,
  AssureCompanyAccountRequest,
  OAuth2,
  OAuthGrantType,
  OAuthMessage,
  PartnerCompanyAccountInfo,
} from "@salaxy/core";

/** Controller for impersonation and federation. */
class ControllerFactory {

  public controller = this.router();

  // Stores user data in this site
  private userDatabase = "dist/www-server/storage/users.json";
  // Certificate store
  private certificateStore = "dist/www-server/storage/certificates";
  // The thumbprint of the Salaxy certificate which is used for impersonation
  private salaxyCertificateThumbprint = "dc68d8bc89043173fd316f34b93599085147c74f";
  // Our own Salaxy account id
  private ownSalaxyAccountId = "FI14POYT0002345678";

  /**
   *  Please note that in this sample we give the userId as a parameter.
   *  In the real life, however, the userId must be obtained in a secure way after proper authorization.
   */
  public router(): Router {

    const ctrl = Router();

    // Federate the given user and redirect the user with the new token to the site root
    ctrl.get("/:userId", (req: Request, res: Response) => {
      // Get userId parameter
      const { userId } = req.params;
      // Federate the user
      return this.federate(userId)
        .then((token) => {
          // Redirect to the site root with the new token
          return res.redirect("/#access_token=" + token);
        });
    });

    return ctrl;
  }

  /** Federates the given company user and returns a new token for the user. */
  private federate(userId: string) {
    // First assure that the company will be created if it does not exist
    return this.assureCompanyAccount(userId)
      .then((company) => {
        // Create assertion token for the company using Salaxy account id
        const assertionToken = this.getAssertionToken(company.salaxyAccountId);
        // Request a new token for the company using the assertion token
        return this.getToken(assertionToken);
      })
      .then((tokenMessage) => {
        // Return the received token
        return tokenMessage.access_token;
      });
  }

  /** Create assertion token for given Salaxy account id */
  private getAssertionToken(salaxyAccountId: string): string {
    // The thumbprint of the certificate
    const thumbprint = this.salaxyCertificateThumbprint;
    // This can be partner specific
    const issuer = "http://tempuri.org";
    const expires = new Date(Date.now() + (10 * 24 * 60 * 60 * 1000));
    const baseFilePath = path.resolve(this.certificateStore, thumbprint);
    const crt = fs.readFileSync(baseFilePath + ".crt");
    const pvk = fs.readFileSync(baseFilePath + ".pvk");

    const assertionToken = CertificateUtility.createAssertionToken(thumbprint, crt, pvk, issuer, expires, salaxyAccountId);

    return assertionToken;
  }

  /** Get bearer token using assertion */
  private getToken(assertionToken: string): Promise<OAuthMessage> {
    const oauth2 = new OAuth2(this.getAjax());
    const oAuthRequest: OAuthMessage = {
      grant_type: OAuthGrantType.JwtBearer,
      assertion: assertionToken,
    };
    return oauth2.token(oAuthRequest);
  }

  /** Assures that the company will be created if it does not exist */
  private assureCompanyAccount(userId: string): Promise<UserInfo> {

    // Check if the user exists in our own database
    const user = this.getUserInfo(userId);
    if (user == null) {
      throw Error("this should never happen");
    }

    // Create assertion token for self
    const assertionToken = this.getAssertionToken(this.ownSalaxyAccountId);

    // Use assertion token to generate a new token for self
    return this.getToken(assertionToken)
      .then((tokenMessage) => {

        // Configure the API client with the token
        const ajax = this.getAjax();
        ajax.setCurrentToken(tokenMessage.access_token);

        // Create request object
        const assureCompanyAccountRequest: AssureCompanyAccountRequest = {
          officialId: user.officialId,
          partnerAccountId: user.userId,
        };
        // Post data to API
        return ajax.postJSON("/partner/company", assureCompanyAccountRequest);
      })
      .then((partnerCompanyAccountInfo: PartnerCompanyAccountInfo) => {
        // Save the returned data to our own user database
        user.salaxyAccountId = partnerCompanyAccountInfo.accountId;
        this.saveUserInfo(user);
        return user;
      });
  }

  /** Returns the API client */
  private getAjax(): AjaxNode {
    const ajax = new AjaxNode();
    ajax.serverAddress = "https://test-api.salaxy.com"; // For production use: https://secure-salaxy-com
    return ajax;
  }

  /** Returns a user from the user database. */
  private getUserInfo(userId: string): UserInfo {
    const data = fs.readFileSync(path.resolve(this.userDatabase));
    const users = JSON.parse(data.toString("utf8")) as UserInfo[];
    for (const user of users) {
      if (user.userId === userId) {
        return user;
      }
    }
    return null;
  }

  /** Saves user to the user database. */
  private saveUserInfo(user: UserInfo): void {
    const data = fs.readFileSync(path.resolve(this.userDatabase));
    const users = JSON.parse(data.toString("utf8")) as UserInfo[];

    for (let i = 0; i < users.length; i++) {
      if (users[i].userId === user.userId) {
        users[i] = user;
        break;
      }
    }
    fs.writeFileSync(path.resolve(this.userDatabase), JSON.stringify(users));

  }
}
export const federationController = new ControllerFactory().controller;
