/** User database entity. */
export class UserInfo {
    public userId: string;
    public officialId: string;
    public salaxyAccountId: string;
}
