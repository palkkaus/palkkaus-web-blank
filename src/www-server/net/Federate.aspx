﻿<%@ Import Namespace="System.IdentityModel.Tokens" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Security.Claims" %>
<%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="c#" runat="server">

        // Stores user data in this site
        private string userDatabase = "storage/users.json";
        // Certificate store
        private string certificateStore = "storage/certificates";
        // The thumbprint of the Salaxy certificate which is used for impersonation
        private string salaxyCertificateThumbprint = "dc68d8bc89043173fd316f34b93599085147c74f";
        //Password for pfx
        private string salaxyCertificatePassword = "salaxy";
        // Our own Salaxy account id
        private string ownSalaxyAccountId = "FI14POYT0002345678";


        /** User mapping class */
        public class UserInfo
        {
            public string userId;
            public string officialId;
            public string salaxyAccountId;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Request.Params["userId"];
            if (userId != null)
            {
                var token = federate(userId);
                Response.Redirect("http://localhost:9001/#access_token=" + token);
            }
            else
            {
                Response.Write("Please give userId -parameter in the query string.");
            }
        }

        /** Utility method for creating an assertion token from the certificate for given account */
        private string createAssertionToken(byte[] certificateBytes, string password, string issuer, DateTime expires, string accountId)
        {
            //Load certificate
            var certificate = new X509Certificate2(certificateBytes, password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);

            //Make header, add signing certificate
            var header = new JwtHeader(new X509SigningCredentials(certificate));
            header.Add(JwtHeaderParameterNames.X5c, new string[] { Convert.ToBase64String(certificate.RawData) });

            //Make payload
            var payload = new JwtPayload(
                issuer,
                "https://test-api.salaxy.com/oauth2/token",
                new Claim[] { new Claim("sub", accountId) },
                null,
                expires);

            //Make token and sign
            var token = new JwtSecurityToken(header, payload);

            //Serialize
            var tokenHandler = new JwtSecurityTokenHandler();
            string jwt = tokenHandler.WriteToken(token);
            return jwt;
        }

        /** Federates the given company user and returns a new token for the user. */
        private string federate(string userId)
        {
            // First assure that the company will be created if it does not exist
            var company = assureCompanyAccount(userId);
            // Create assertion token for the company using Salaxy account id
            var assertionToken = getAssertionToken(company.salaxyAccountId);
            // Request a new token for the company using the assertion token
            var token = getToken(assertionToken);
            return token;
        }

        /** Create assertion token for given Salaxy account id */
        private string getAssertionToken(string salaxyAccountId)
        {
            // This can be partner specific
            var issuer = "http://tempuri.org";
            var expires = DateTime.Now.AddMilliseconds(10 * 24 * 60 * 60 * 1000);
            var certFile = certificateStore + @"\" + salaxyCertificateThumbprint + ".pfx";
            var bytes = File.ReadAllBytes(Path.Combine(Server.MapPath("."),"../"+certFile));
            var assertionToken = createAssertionToken(bytes, salaxyCertificatePassword, issuer, expires, salaxyAccountId);
            return assertionToken;
        }

        /** Get bearer token using assertion */
        private string getToken(string assertionToken)
        {
            var oAuthRequest = new
            {
                grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer",
                assertion = assertionToken
            };
            var wc = new WebClient();
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            wc.Encoding = Encoding.UTF8;
            var tokenUrl = "https://test-api.salaxy.com/oauth2/token";

            var tokenResponseJson = wc.UploadString(tokenUrl, JsonConvert.SerializeObject(oAuthRequest));
            dynamic tokenResponse = JsonConvert.DeserializeObject(tokenResponseJson);
            return tokenResponse.access_token;
        }

        /** Assures that the company will be created if it does not exist */
        private UserInfo assureCompanyAccount(string userId)
        {

            // Check if the user exists in our own database
            var user = getUserInfo(userId);
            if (user == null)
            {
                throw new Exception("this should never happen");
            }

            // Create assertion token for self
            var assertionToken = getAssertionToken(ownSalaxyAccountId);

            // Use assertion token to generate a new token for self
            var token = getToken(assertionToken);

            // Create request object
            var assureCompanyAccountRequest = new
            {
                officialId = user.officialId,
                partnerAccountId = user.userId
            };
            // Post data to API
            var wc = new WebClient();
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            wc.Encoding = Encoding.UTF8;
            var assureCompanyUrl = "https://test-api.salaxy.com/v02/api/partner/company";

            var assureCompanyResponseJson = wc.UploadString(assureCompanyUrl, JsonConvert.SerializeObject(assureCompanyAccountRequest));

            dynamic assureCompanyResponse = JsonConvert.DeserializeObject(assureCompanyResponseJson);

            user.salaxyAccountId = assureCompanyResponse.accountId;
            saveUserInfo(user);
            return user;
        }

        /** Returns a user from the user database. */
        private UserInfo getUserInfo(string userId)
        {
            var data = File.ReadAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase));
            var users = JsonConvert.DeserializeObject<UserInfo[]>(data);
            foreach (var user in users)
            {
                if (user.userId == userId)
                {
                    return user;
                }
            }
            return null;
        }

        /** Saves user to the user database. */
        private void saveUserInfo(UserInfo user)
        {
            var data = File.ReadAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase));
            var users = JsonConvert.DeserializeObject<UserInfo[]>(data);

            for (var i = 0; i < users.Length; i++)
            {
                if (users[i].userId == user.userId)
                {
                    users[i] = user;
                    break;
                }
            }
            File.WriteAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase), JsonConvert.SerializeObject(users));
        }
    
</script>
