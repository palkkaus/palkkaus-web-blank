module.exports = function(grunt) {
    require('jit-grunt')(grunt);
   

    grunt.initConfig({
        conf: {
            gruntServerPort: 9001,
        },
        less: {
            client: {
                options: {
                    sourceMap: true,
                    sourceMapFileInline: false,
                    sourceMapRootpath: "/",
                },
                files: {
                    "dist/www/css/bootstrap.css": "src/www/less/_bootstrap.less",
                    "dist/www/css/salaxy-lib-ng1-bootstrap.css": "src/www/less/_salaxy-component-bootstrap.less",   
                    "dist/www/css/salaxy-rpt.css": "src/www/less/_salaxy-rpt.less"     
                }
            }
        },
        postcss: {
            client: {
                options: {
                    map: {
                        inline: false,
                    },
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')(),
                    ]
                },
                files: {
                    "dist/www/css/bootstrap.css": "dist/www/css/bootstrap.css",
                    "dist/www/css/salaxy-lib-ng1-bootstrap.css": "dist/www/css/salaxy-lib-ng1-bootstrap.css"
                }
            },

        },

        ts: {
            client: {
                src: [
                    'src/www/ts/**/*.ts'
                ],
                out: 'dist/www/js/Salaxy.app.js',
                options: {
                    module: 'amd',
                    target: 'es5',
                    sourceMap: false,
                    declaration: false,
                    removeComments: false,
                    moduleResolution: "node",
                }
            },
            server: {
                src: [
                    'src/www-server/ts/**/*.ts'
                ],
                outDir: 'dist/www-server/js',
                options: {
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: false,
                    declaration: false,
                    removeComments: false,
                    moduleResolution: "node",
                    rootDir: "./src/www-server/ts"
                }
            },
        },
        copy: {
            ng1: {
                files: [
                    { expand: true, cwd: 'node_modules/@salaxy/ng1/js', src: 'salaxy-lib-ng1-all.js', dest: 'dist/www/js/' },
                ],
            },
            client: {
                files: [
                    { expand: true, cwd: 'src/www', src: ['app/**','img/**','favicon.ico','index.html'], dest: 'dist/www/' },
                ],
            },
            server: {
                files: [
                    { expand: true, cwd: 'src/www-server', src: ['storage/**'], dest: 'dist/www-server/' },
                ],
            },
        },
        express: {
            server: {
                options: {
                  port: '<%= conf.gruntServerPort%>',
                  server: 'dist/www-server/js/server.js',
                  bases: 'dist/www',
                  hostname: "localhost",
                  livereload: true,
                }
              }
        },
        watch: {
            html: {
                files: ['src/www/app/**/*.html', 'src/www/*.html'],
                tasks: ['copy'],
                options: {
                    livereload: true,
                },
            },
            less: {
                files: ['src/www/less/**/*.less'],
                tasks: ['less','postcss'],
                options: {
                    livereload: true,
                },
            },
            client: {
                files: ['src/www/ts/**/*.ts'],
                tasks: ['ts:client'],
                options: {
                    livereload: true
                },
            },
            server: {
                files: ['src/www-server/ts/**/*.ts'],
                tasks: ['ts:server'],
                options: {
                    livereload: true
                },
            },
        },
        open: {
            all: {
                path: 'http://<%= express.server.options.hostname %>:<%= express.server.options.port %>'
            }
        },
    });

    // jit-grunt cannot handle grunt-express
    grunt.loadNpmTasks('grunt-express');

    // ===========================================================================
    // BUILD JOBS 		  ========================================================
    // =========================================================================== 
    var tasks = ['less','postcss', 'copy','ts'];
   
    grunt.registerTask('default', tasks);
    grunt.registerTask('build', tasks);
    grunt.registerTask('server', ['express','open','watch']);
};